import React from 'react'
import styled from 'styled-components'

const StyledHeader = styled.header`
  font-size: 56px;
  text-align: center;
  font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New',
    monospace;
  padding: 22px 0;
`

const Header = () => <StyledHeader>&lt;IMGrid /&gt;</StyledHeader>

export default Header
