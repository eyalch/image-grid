import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import { Image } from './typings/image'

const StyledGrid = styled.ul`
  list-style: none;
  margin: 0;
  padding: 0;
  display: grid;
  grid-template-columns: repeat(auto-fill, 300px);
  justify-content: center;
  grid-gap: 30px;
  padding-bottom: 30px;
`
const StyledImage = styled.li`
  img {
    width: 300px;
    height: 300px;
    object-fit: cover;
    display: block;
  }

  small {
    font-size: 11px;
  }
`

const ImageGrid = () => {
  const [images, setImages] = useState<Image[]>([])

  useEffect(() => {
    fetch('https://picsum.photos/v2/list')
      .then(res => res.json())
      .then(data => setImages(data))
  }, [])

  return (
    <StyledGrid>
      {images.map(image => (
        <StyledImage key={image.id}>
          <img src={image.download_url} />
          <small>By: {image.author}</small>
        </StyledImage>
      ))}
    </StyledGrid>
  )
}

export default ImageGrid
