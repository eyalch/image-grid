import React from 'react'
import Header from './Header'
import ImageGrid from './ImageGrid'

const App = () => (
  <>
    <Header />
    <ImageGrid />
  </>
)

export default App
